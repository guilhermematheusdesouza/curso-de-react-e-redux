export default function () {
  return [
    { title: 'Javascript: The Good Parts', pages: 10 },
    { title: 'Harry Potter', pages: 3 },
    { title: 'The Dark Tower', pages: 13 },
    { title: 'Eloquent Ruby', pages: 32 }
  ]
}